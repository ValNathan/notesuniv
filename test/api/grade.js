'use strict';

const {expect} = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixtures = require('../fixtures/grades');

const server = request(createServer());

describe('Grade api', function(){
    describe('POST /api/v1/grades', function(){
        before(async function() {
            await database.sequelize.query('DELETE from GRADES');
            const {Grades} = database;
            await Grades.create(fixtures);  
        });
    
        it("La requête envoie toutes les données d'une grade. On reçoit 201 lorsqu'il est crée", async () => {
            await server.post('/api/v1/grades')
                .send({
                    idGrade: "Reactpdw2019-johndoe20000201",
                    grade: 12,
                    subjectId: "Reactlpdw2019",
                    studentId: "johndoe20000201"
                })
                .expect(201);
        });
    
        it("La requete envoie n'evoie pas tous les données d'une grade, on reçoit un 400", async () => {
            await server.post('/api/v1/grades')
                .send({
                    idGrade: "Reactpdw2019-johndoe20000201",
                })
                .expect(400);
        });
    
        it("La requete envoie une grade qui existe déjà, on reçoit un 409", async () => {
            await server.post('/api/v1/grades')
                .send({
                    idGrade: "Alglpdw2019-johndoe20000201",
                    grade: 12,
                    subjectId: "Alglpdw2019",
                    studentId: "johndoe20000201"
                })
                .expect(409);
        });
    });

    describe('GET /api/v1/grades', function() {
        it("Retourne la liste de tous les grades", async () => {
            const {body: grades} = await server
                .get('/api/v1/grades')
                .set('Accept', 'application/json')
                .expect(200);
            expect(grades).to.be.an('array');
            expect(grades.length).to.equal(grades.length);
        });
    });

    describe('GET /api/v1/grades/:idGrade', function(){
        it("L'idGrade donné n'existe pas. Erreur 404", async () => {
            await server.get('/api/v1/grades/je-n-existe-pas')
                .expect(404);
        });

        it("La grade donné existe. Code 200", async () => {
			const {body: grade} = await server.get('/api/v1/grades/Alglpdw2019-johndoe20000201')
				.expect(200);

            expect(grade.idGrade).to.equal('Alglpdw2019-johndoe20000201');
			expect(grade.grade).to.equal(12);
			expect(grade.subjectId).to.equal('Alglpdw2019');
			expect(grade.studentId).to.equal('johndoe20000201');
		});
    });

    describe('PUT /api/v1/grades/:idGrade', function() {
		it('Update une grade en fonction de son idGrade', async () => {
			await server.put('/api/v1/grades/Alglpdw2019-johndoe20000201')
			.send({
				idGrade: "Alglpdw2019-johndoe20000201",
				grade: 16,
                subjectId: "Alglpdw2019",
                studentId: "johndoe20000201"
			})
			.expect(201);
			const {body: grade} = await server.get('/api/v1/grades/Alglpdw2019-johndoe20000201');
			expect(grade.grade).to.equal(16);
		});
	});
    
    describe('DELETE /api/v1/grades/:idGrade', function(){
        it("La grade a été supprimée. Code 204", async () => {
            await server.delete('/api/v1/grades/Alglpdw2019-johndoe20000201').expect(204);
            const {body: grades} = await server.get('/api/v1/grades/Alglpdw2019-johndoe20000201')
            .expect(404);
        });
    });

});
