'use strict';

const {expect} = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixtures = require('../fixtures/subjects');

const server = request(createServer());

describe('Subject api', function(){

    describe('POST /api/v1/subjects', function() {
        before(async function() {
            await database.sequelize.query('DELETE from Subjects');
            const {Subjects} = database;
            await Subjects.create(fixtures);
        });

        it("La requete envoie toutes les données d'une nouvelle matière. Cette matière est créée et on reçoit un code 201", async () => {
            await server.post('/api/v1/subjects')
                .send({
                    subjectId: 'APIlpdw2019',
                    subjectName: 'API'
                })
                .expect(201);
        });

        it("La requete n'envoie pas toutes les données d'une matière. On reçoit un 400", async () => {
            await server.post('/api/v1/subjects')
                .send({
                    subjectId: 'Moblpdw2019'
                })
                .expect(400);
        });

        it("La requête envoie une matière déjà existante. On reçoit un 409", async () => {
            await server.post('/api/v1/subjects')
                .send({
                    subjectId: 'APIlpdw2019',
                    subjectName: 'API'
                })
                .expect(409);
        });
    })

    describe('GET /api/v1/subjects', function() {
        it("Retourne la liste de toutes les matières", async () => {
            const {body: subjects} = await server
                .get('/api/v1/subjects')
                .set('Accept', 'application/json')
                .expect(200);
            expect(subjects).to.be.an('array');
            expect(subjects.length).to.equal(subjects.length);
        });

        it("Retourne une matière spécifique", async () => {
            const {body: subjects} = await server
                .get('/api/v1/subjects')
                .query({
                    subjectId: 'APIlpdw2019',
                    subjectName: 'API'
                })
                .set('Accept', 'application/json')
                .expect(200);
            expect(subjects).to.be.an('array');
            expect(subjects.length).to.equal(1);
            expect(subjects[0].subjectId).to.equal('APIlpdw2019');
        });
    });

    describe('GET /api/v1/subjects/:subjectId', function(){
        it("La matière donnée n'existe pas. Erreur 404", async () => {
            await server.get('/api/v1/subjects/je-n-existe-pas')
                .expect(404);
        });

        it("La matière donnée existe. Code 200", async () => {
			const {body: subject} = await server.get('/api/v1/subjects/APIlpdw2019')
				.expect(200);

            expect(subject.subjectId).to.equal('APIlpdw2019');
			expect(subject.subjectName).to.equal('API');
		});
    });

    describe('PUT /api/v1/subjects/:subjectId', function () {
        it('Update une matière', async () => {
            await server.put('/api/v1/subjects/APIlpdw2019')
            .send({
                subjectId: "APIlpdw2019",
                subjectName: "API et Yaml"
            })
            .expect(201);
            const {body: subject} = await server.get('/api/v1/subjects/APIlpdw2019');
            expect(subject.subjectName).to.equal("API et Yaml");
        });

        it("La matière n'existe pas. Code 404", async () => {
            await server.put('/api/v1/subjects/je-n-existe-pas')
                .expect(404);
        });
    });
});