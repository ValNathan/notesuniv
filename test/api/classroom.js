'use strict';

const {expect} = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixtures = require('../fixtures/classrooms');

const server = request(createServer());

describe('Classroom api', function() {
    describe('POST /api/v1/classrooms', function () {
		before(async function() {
			await database.sequelize.query('DELETE from CLASSROOMS');
			const {Classrooms} = database;
			await Classrooms.create(fixtures);
        });

        it("La requete envoie tous les données d'une nouvelle classroom, la classroom est crée et on reçoit un 201", async () => {
			await server.post('/api/v1/classrooms')
				.send({
					idClassroom: '2018-2019-LPDW19',
                    name: 'Licence Pro Développement Web',
                    year: 2018
				})
				.expect(201);
		});

		it("La requete n'envoie pas toutes les données d'une classroom, on reçoit un 400", async () => {
			await server.post('/api/v1/classrooms')
				.send({
					idClassroom: '2019-2020-LPDW20'
				})
				.expect(400);
		});

		it("La requete envoie une classroom qui existe déjà, on reçoit un 409", async () => {
			await server.post('/api/v1/classrooms')
				.send({
					idClassroom: "2019-2020-LPDW20",
    				name: "Licence Pro Développement Web",
    				year: 2019
				})
				.expect(409);
		});
	})
	
	describe('GET /api/v1/classrooms', function() {
		it("Retourne la liste de toutes les classes", async () => {
			const {body: classrooms} = await server
				.get('/api/v1/classrooms')
				.set('Accept', 'application/json')
				.expect(200);
			expect(classrooms).to.be.an('array');
			expect(classrooms.length).to.equal(2);
		});

		it("Retourne une classe specifique", async () => {
			const {body: classrooms} = await server
				.get('/api/v1/classrooms')
				.query({
					idClassroom: '2018-2019-LPDW19',
                    name: 'Licence Pro Développement Web',
                    year: 2018
				})
				.set('Accept', 'application/json')
				.expect(200);

			expect(classrooms).to.be.an('array');
			expect(classrooms.length).to.equal(1);
			expect(classrooms[0].idClassroom).to.equal('2018-2019-LPDW19');
		});
	});

	describe('GET /api/v1/classrooms/:idClassroom', function() {
		it("L'idClassroom donné n'existe pas alors j'ai un 404", async () => {
			await server.get('/api/v1/classrooms/je-n-existe-pas')
				.expect(404);
		});

		it("L'idClassroom donné existe donc j'ai un 200 avec une classroom", async () => {
			const {body: classroom} = await server.get('/api/v1/classrooms/2018-2019-LPDW19')
				.expect(200);

			expect(classroom.idClassroom).to.equal('2018-2019-LPDW19');
			expect(classroom.name).to.equal('Licence Pro Développement Web');
			expect(classroom.year).to.equal(2018);
		});
	});
	
	describe('PUT /api/v1/classrooms/:idClassroom', function() {
		it('Update une classroom en fonction de son classroomId', async () => {
			await server.put('/api/v1/classrooms/2018-2019-LPDW19')
			.send({
				idClassroom: "2018-2019-LPDW19",
				name: "Licence Pro Développement Web",
				year: 2056
			})
			.expect(201);
			const {body: classroom} = await server.get('/api/v1/classrooms/2018-2019-LPDW19');
			expect(classroom.year).to.equal(2056);
		});

		it("L'idClassroom donné n'existe pas alors j'ai un 404", async () => {
			await server.put('/api/v1/classrooms/je-n-existe-pas')
				.expect(404);
		});
	});
});