'use strict';

const {expect} = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixtures = require('../fixtures/teachers');

const server = request(createServer());

describe('Teacher api', function(){

    describe('POST /api/v1/teachers', function(){
        before(async function() {
            await database.sequelize.query('DELETE from Teachers');
            const {Teachers} = database;
            await Teachers.create(fixtures);  
        });
    
        it("La requête envoie toutes les données d'un teacher. On reçoit 201 lorsqu'il est crée", async () => {
            await server.post('/api/v1/teachers')
                .send({
                    teacherId: 'lpdw2019dagval',
                    teacherLastName: 'daguenet',
                    teacherFirstName: 'valentin'
                })
                .expect(201);
        });
    
       it("La requete envoie n'evoie pas tous les données d'un teacher, on reçoit un 400", async () => {
            await server.post('/api/v1/teachers')
                .send({
                    teacherId: 'lpdw2019riclau'
                })
                .expect(400);
        });
    
        it("La requete envoie un professeur qui existe déjà, on reçoit un 409", async () => {
            await server.post('/api/v1/teachers')
                .send({
                    teacherId: 'lpdw2019riclau',
                    teacherLastName: 'ricard',
                    teacherFirstName: 'laurent'
                })
                .expect(409);
        });
    });

    describe('GET /api/v1/teachers', function() {
        it("Retourne la liste de tous les professeurs", async () => {
            const {body: teachers} = await server
                .get('/api/v1/teachers')
                .set('Accept', 'application/json')
                .expect(200);
            expect(teachers).to.be.an('array');
            expect(teachers.length).to.equal(teachers.length);
        });
    });

    describe('GET /api/v1/teachers/:teacherId', function(){
        it("Le professeur donné n'existe pas. Erreur 404", async () => {
            await server.get('/api/v1/teachers/je-n-existe-pas')
                .expect(404);
        });

        it("Le professeur donné existe. Code 200", async () => {
			const {body: teacher} = await server.get('/api/v1/teachers/lpdw2019riclau')
				.expect(200);

            expect(teacher.teacherId).to.equal('lpdw2019riclau');
			expect(teacher.teacherLastName).to.equal('ricard');
			expect(teacher.teacherFirstName).to.equal('laurent');
		});
    });
    
    describe('DELETE /api/v1/teachers/:teacherId', function(){
        it("Le professeur donné n'existe pas. Erreur 404", async () => {
            await server.delete('/api/v1/teachers/je-n-existe-pas').expect(404);
        });

        it("Le professeur a été remercié. Code 204", async () => {
            await server.delete('/api/v1/teachers/lpdw2019riclau').expect(204);
            const {body: teachers} = await server.get('/api/v1/teachers/lpdw2019riclau')
            .expect(404);
        });
    });

});
