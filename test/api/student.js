'use strict';

const {expect} = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixtures = require('../fixtures/students');

const server = request(createServer());

describe('Student api', function(){
    describe('POST /api/v1/students', function(){
        before(async function() {
            await database.sequelize.query('DELETE from STUDENTS');
            const {Students} = database;
            await Students.create(fixtures);  
        });
    
        it("La requête envoie toutes les données d'un student. On reçoit 201 lorsqu'il est crée", async () => {
            await server.post('/api/v1/students')
                .send({
                    studentId: 'janedoe20000201',
                    lastname: 'doe',
                    firstname: 'jane',
                    dateOfBirth: "2000-02-01"
                })
                .expect(201);
        });
    
        it("La requete envoie n'evoie pas tous les données d'un student, on reçoit un 400", async () => {
            await server.post('/api/v1/students')
                .send({
                    studentId: 'johndoe20000201'
                })
                .expect(400);
        });
    
        it("La requete envoie un etudiant qui existe déjà, on reçoit un 409", async () => {
            await server.post('/api/v1/students')
                .send({
                    studentId: 'johndoe20000201',
                    lastname: 'doe',
                    firstname: 'john',
                    dateOfBirth: "2000-02-01"
                })
                .expect(409);
        });
    });

    describe('GET /api/v1/students', function() {
        it("Retourne la liste de tous les etudiants", async () => {
            const {body: students} = await server
                .get('/api/v1/students')
                .set('Accept', 'application/json')
                .expect(200);
            expect(students).to.be.an('array');
            expect(students.length).to.equal(students.length);
        });
    });

    describe('GET /api/v1/students/:studentId', function(){
        it("L'etudiant donné n'existe pas. Erreur 404", async () => {
            await server.get('/api/v1/students/je-n-existe-pas')
                .expect(404);
        });

        it("L'etudiant donné existe. Code 200", async () => {
			const {body: student} = await server.get('/api/v1/students/johndoe20000201')
				.expect(200);

            expect(student.studentId).to.equal('johndoe20000201');
			expect(student.lastname).to.equal('doe');
			expect(student.firstname).to.equal('john');
			expect(student.dateOfBirth).to.equal('2000-02-01');
		});
    });
    
    describe('DELETE /api/v1/students/:studentId', function(){
        it("L'etudiant donné n'existe pas. Erreur 404", async () => {
            await server.delete('/api/v1/students/je-n-existe-pas').expect(404);
        });

        it("L'etudiant a été remercié. Code 204", async () => {
            await server.delete('/api/v1/students/johndoe20000201').expect(204);
            const {body: students} = await server.get('/api/v1/students/johndoe20000201')
            .expect(404);
        });
    });

});
