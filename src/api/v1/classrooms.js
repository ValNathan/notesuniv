const express = require('express');
const request = require('request-promise');
const router = express.Router();

/* POST new classroom */
router.post('/', async(req, res) => {
    try {
		// Vérifier que il y a un idClassroom, name, year
		const body = req.body;
		if (body.idClassroom && body.name && body.year) {
			// Insert dans la bdd
			const {Classrooms} = req.db;
			const classroom = await Classrooms.create(body);
			return res.status(201).send(classroom);
		}
		else {
			return res.status(400).send({message: 'Missing data'});
		}	
	} catch (err) {
		if (err.name === 'SequelizeUniqueConstraintError') {
			return res.status(409).send({message: 'Classroom exists déjà'})
		}
	}
    
});

/* GET classroom listing*/
router.get('/', async (req, res) => {
	const {idClassroom, name, year} = req.query;
	const filter = {
		where: {}
	};
	if (idClassroom) filter.where.idClassroom = idClassroom;
	if (name) filter.where.name = name;
	if (year) filter.where.year = year;

	const {Classrooms} = req.db;
	const classrooms = await Classrooms.findAll(filter);

	res.send(classrooms);
});

/* GET specific classroom */
router.get('/:idClassroom', async (req, res) => {
	const idClassroom = req.params.idClassroom;
	const {Classrooms} = req.db;
	const classroom = await Classrooms.findOne({ where: {idClassroom: idClassroom} });
	if (classroom) {
		return res.send(classroom);
	} else {
		return res.status(404)
			.send({message: `idClassroom ${idClassroom} not found`});
	}
});

/* PUT Update classroom */
router.put('/:idClassroom', async (req, res) => {
	const idClassroom = req.params.idClassroom;
	const {Classrooms} = req.db;
	const classroom = await Classrooms.findOne({ where: {idClassroom: idClassroom} });
	if (classroom) {
		Classrooms.update({
			idClassroom: req.body.idClassroom,
			name: req.body.name,
			year: req.body.year
		},{ 
			where: { idClassroom: req.params.idClassroom } 
		})
		.then(result => {
			res.status(201).json(result);
		});
	} else {
		return res.status(404)
			.send({message: `idClassroom ${idClassroom} not found`});
	}
});

module.exports = router;