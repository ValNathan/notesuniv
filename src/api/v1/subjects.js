const express = require('express');
const request = require('request-promise');
const router = express.Router();

/* POST new subject */
router.post('/', async(req, res) => {
    try {
		// Vérifier que il y a un idClassroom, name, year
		const body = req.body;
		if (body.subjectId && body.subjectName) {
			// Insert dans la bdd
			const {Subjects} = req.db;
			const subject = await Subjects.create(body);
			return res.status(201).send(subject);
		}
		else {
			return res.status(400).send({message: 'Missing data'});
		}	
	} catch (err) {
		if (err.name === 'SequelizeUniqueConstraintError') {
			return res.status(409).send({message: 'Subject exists déjà'})
		}
	}

    
});

/* GET subject listing*/
router.get('/', async (req, res) => {
	const {subjectId, subjectName} = req.query;
	const filter = {
		where: {}
	};
	if (subjectId) filter.where.subjectId = subjectId;
	if (subjectName) filter.where.subjectName = subjectName;

	const {Subjects} = req.db;
	const subjects = await Subjects.findAll(filter);

	res.send(subjects);
});

/* GET specific subject */
router.get('/:subjectId', async (req, res) => {
	const subjectId = req.params.subjectId;
	const {Subjects} = req.db;
	const subject = await Subjects.findOne({ where: {subjectId: subjectId} });
	if (subject) {
		return res.send(subject);
	} else {
		return res.status(404)
			.send({message: `subjectId ${subjectId} not found`});
	}
});

/* PUT Update subject */
router.put('/:subjectId', async (req, res) => {
	const subjectId = req.params.subjectId;
	const {Subjects} = req.db;
	const subject = await Subjects.findOne({ where: {subjectId: subjectId} });
	if (subject) {
		Subjects.update({
			subjectId: req.body.subjectId,
			subjectName: req.body.subjectName
		},{ 
			where: { subjectId: req.params.subjectId } 
		})
		.then(result => {
			res.status(201).json(result);
		});
	} else {
		return res.status(404)
			.send({message: `subjectId ${subjectId} not found`});
	}
});

module.exports = router;
