const express = require('express');
const request = require('request-promise');
const router = express.Router();

/* POST new subject */
router.post('/', async(req, res) => {
    try {
		// Vérifier qu'il y a un nom, prenom et id
		const body = req.body;
		if (body.teacherId && body.teacherLastName && body.teacherFirstName) {
			// Insert dans la bdd
			const {Teachers} = req.db;
			const teacher = await Teachers.create(body);
			return res.status(201).send(teacher);
		}
		else {
			return res.status(400).send({message: 'Missing data'});
		}	
	} catch (err) {
		if (err.name === 'SequelizeUniqueConstraintError') {
			return res.status(409).send({message: 'Teacher exists déjà'})
		}
	}
    
});

router.get('/', async(req, res) => {
	const {teacherId, teacherLastName, teacherFirstName} = req.query;
	const filter = {
		where: {}
	};
	if(teacherId) filter.where.teacherId = teacherId;
	if(teacherLastName) filter.where.teacherLastName = teacherLastName;
	if(teacherFirstName) filter.where.teacherFirstName = teacherFirstName;

	const {Teachers} = req.db;
	const teachers = await Teachers.findAll(filter);

	res.send(teachers);
});

router.get('/:teacherId', async (req, res) => {
    const teacherId = req.params.teacherId;
	const {Teachers} = req.db;
	const teacher = await Teachers.findOne({ where: {teacherId: teacherId}  });
	if (teacher) {
		return res.send(teacher);
	} else {
		return res.status(404)
			.send({message: `Teacher ${teacherId} not found`});
	}
});

//DELETE teacher

router.delete('/:teacherId', async (req, res) => {
	const teacherId = req.params.teacherId;
	const {Teachers} = req.db;
	const teacher = await Teachers.findOne({ where: {teacherId: teacherId} });
	if (teacher) {
		Teachers.destroy({ 
			where: { teacherId: req.params.teacherId } 
		})
		.then(result => {
			res.status(204).json(result);
		});
	} else {
		return res.status(404)
			.send({message: `teacherId ${teacherId} not found`});
	}
});

module.exports = router;