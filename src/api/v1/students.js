const express = require('express');
const request = require('request-promise');
const router = express.Router();

/* POST new student */
router.post('/', async(req, res) => {
    try {
		// Vérifier qu'il y a un nom, prenom et id
		const body = req.body;
		if (body.studentId && body.lastname && body.firstname && body.dateOfBirth) {
			// Insert dans la bdd
			const {Students} = req.db;
			const student = await Students.create(body);
			return res.status(201).send(student);
		}
		else {
			return res.status(400).send({message: 'Missing data'});
		}	
	} catch (err) {
		if (err.name === 'SequelizeUniqueConstraintError') {
			return res.status(409).send({message: 'Student exists déjà'})
		}
	}
    
});

router.get('/', async(req, res) => {
	const {studentId, lastname, firstname, dateOfBirth} = req.query;
	const filter = {
		where: {}
	};
	if(studentId) filter.where.studentId = studentId;
	if(lastname) filter.where.lastname = lastname;
	if(firstname) filter.where.firstname = firstname;
	if(dateOfBirth) filter.where.dateOfBirth = dateOfBirth;

	const {Students} = req.db;
	const students = await Students.findAll(filter);

	res.send(students);
});

router.get('/:studentId', async (req, res) => {
    const studentId = req.params.studentId;
	const {Students} = req.db;
	const student = await Students.findOne({ where: {studentId: studentId}  });
	if (student) {
		return res.send(student);
	} else {
		return res.status(404)
			.send({message: `Student ${studentId} not found`});
	}
});

//DELETE student

router.delete('/:studentId', async (req, res) => {
	const studentId = req.params.studentId;
	const {Students} = req.db;
	const student = await Students.findOne({ where: {studentId: studentId} });
	if (student) {
		Students.destroy({ 
			where: { studentId: req.params.studentId } 
		})
		.then(result => {
			res.status(204).json(result);
		});
	} else {
		return res.status(404)
			.send({message: `studentId ${studentId} not found`});
	}
});

module.exports = router;
