const express = require('express');
const request = require('request-promise');
const router = express.Router();

/* POST new grade */
router.post('/', async(req, res) => {
    try {
		// Vérifier qu'il y a un idGrade, une grade, un subjectId et un studentId
		const body = req.body;
		if (body.idGrade && body.grade && body.subjectId && body.studentId) {
			// Insert dans la bdd
			const {Grades} = req.db;
			const grade = await Grades.create(body);
			return res.status(201).send(grade);
		}
		else {
			return res.status(400).send({message: 'Missing data'});
		}	
	} catch (err) {
		if (err.name === 'SequelizeUniqueConstraintError') {
			return res.status(409).send({message: 'Grade exists déjà'})
		}
	}
    
});

/* GET list of grades */
router.get('/', async(req, res) => {
	const {idGrade, grade, subjectId, studentId} = req.query;
	const filter = {
		where: {}
	};
	if(idGrade) filter.where.idGrade = idGrade;
	if(grade) filter.where.grade = grade;
	if(subjectId) filter.where.subjectId = subjectId;
	if(studentId) filter.where.studentId = studentId;

	const {Grades} = req.db;
	const grades = await Grades.findAll(filter);

	res.send(grades);
});

router.get('/:idGrade', async (req, res) => {
    const idGrade = req.params.idGrade;
	const {Grades} = req.db;
	const grade = await Grades.findOne({ where: {idGrade: idGrade}  });
	if (grade) {
		return res.send(grade);
	} else {
		return res.status(404)
			.send({message: `Grade ${idGrade} not found`});
	}
});

/* PUT Update grade */
router.put('/:idGrade', async (req, res) => {
	const idGrade = req.params.idGrade;
	const {Grades} = req.db;
	const grade = await Grades.findOne({ where: {idGrade: idGrade} });
	if (grade) {
		Grades.update({
			idGrade: req.body.idGrade,
			grade: req.body.grade,
			subjectId: req.body.subjectId,
			studentId: req.body.studentId
		},{ 
			where: { idGrade: req.params.idGrade } 
		})
		.then(result => {
			res.status(201).json(result);
		});
	} else {
		return res.status(404)
			.send({message: `idGrade ${idGrade} not found`});
	}
});

//DELETE grade

router.delete('/:idGrade', async (req, res) => {
	const idGrade = req.params.idGrade;
	const {Grades} = req.db;
	const grade = await Grades.findOne({ where: {idGrade: idGrade} });
	if (grade) {
		Grades.destroy({ 
			where: { idGrade: req.params.idGrade } 
		})
		.then(result => {
			res.status(204).json(result);
		});
	} else {
		return res.status(404)
			.send({message: `idGrade ${idGrade} not found`});
	}
});

module.exports = router;
