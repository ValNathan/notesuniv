'use strict';

const Grades = (sequelize, DataTypes) => {
	return sequelize.define('Grades', {
		idGrade: {
			type: DataTypes.STRING,
			primaryKey: true,
			validate: {notEmpty: {msg: '-> Missing idGrade'}}
		},
		grade: {
			type: DataTypes.INTEGER,
			validate: {notEmpty: {msg: '-> Missing grade'}},
			allowNull: false
		},
		subjectId: {
			type: DataTypes.INTEGER,
			validate: {notEmpty: {msg: '-> Missing subjectId'}},
			allowNull: false
		},
		studentId: {
			type: DataTypes.INTEGER,
			validate: {notEmpty: {msg: '-> Missing studentId'}},
			allowNull: false
		}
	});
};

module.exports = Grades;