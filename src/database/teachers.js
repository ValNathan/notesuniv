'use strict';

const Teachers = (sequelize, DataTypes) => {
    return sequelize.define('Teachers', {
        teacherId: {
            type: DataTypes.STRING,
            primaryKey: true,
            validate: {notEmpty: {msg: '-> Missing id'}}
        },
        teacherLastName: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {notEmpty: {msg: '-> Missing lastname'}}
        },
        teacherFirstName: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {notEmpty: {msg: '-> Missing firstname'}}
        }
    });
};

module.exports = Teachers;