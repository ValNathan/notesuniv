'use strict';

const Subjects = (sequelize, DataTypes) => {
    return sequelize.define('Subjects', {
        subjectId: {
            type: DataTypes.STRING,
            primaryKey: true,
            validate: {notEmpty: {msg: '-> missing Id'}}
        },

        subjectName: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {notEmpty: {msg: '-> missing name'}}
        }
    });
};

module.exports = Subjects;