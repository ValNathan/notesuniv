'use strict';
const path = require('path');
const Sequelize = require('sequelize');

const url = (process.env.DATABASE_URL || '').match(/(.*)\:\/\/(.*?)\:(.*)@(.*)\:(.*)\/(.*)/);
const DB_name     = url ? url[6] : null;
const user        = url ? url[2] : null;
const pwd         = url ? url[3] : null;
const protocol    = url ? url[1] : null;
const dialect     = url ? url[1] : 'sqlite';
const port        = url ? url[5] : null;
const host        = url ? url[4] : null;

// Here we verify the environmental variable to know if we're on test, development or production
const storage     = process.env.NODE_ENV === 'test' ? 'database-test.sqlite' : process.env.DATABASE_STORAGE || 'database.sqlite';

const sequelize = new Sequelize(DB_name, user, pwd, {
	dialect,
	protocol,
	port,
	host,
	storage,
	omitNull: true,
	logging: process.env.NODE_ENV !== 'test'
});

sequelize.sync()
	.then(() => {
		console.log('DB loaded');
	})
;

// TODO: Add here all your mapped models of your database

const Teachers = sequelize.import(path.join(__dirname, 'teachers'));
const Subjects = sequelize.import(path.join(__dirname, 'subjects'));
const Classrooms = sequelize.import(path.join(__dirname, 'Classrooms'));
const Students = sequelize.import(path.join(__dirname, 'Students'));
const Grades = sequelize.import(path.join(__dirname, 'Grades'));

// TODO: And export them
exports.Teachers = Teachers;
exports.Subjects = Subjects;
exports.Classrooms = Classrooms;
exports.Students = Students;
exports.Grades = Grades;

// Exporting sequelize object to allow raw queries if needed
exports.sequelize = sequelize;
