'use strict';

const Classrooms = (sequelize, DataTypes) => {
	return sequelize.define('Classrooms', {
		idClassroom: {
			type: DataTypes.STRING,
			primaryKey: true,
			validate: {notEmpty: {msg: '-> Missing idClassroom'}}
		},
		name: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing name'}},
			allowNull: false
		},
		year: {
			type: DataTypes.INTEGER,
			validate: {notEmpty: {msg: '-> Missing year'}},
			allowNull: false
		}
	});
};

module.exports = Classrooms;