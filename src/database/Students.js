'use strict';

const Students = (sequelize, DataTypes) => {
	return sequelize.define('Students', {
		studentId: {
			type: DataTypes.STRING,
			primaryKey: true,
			validate: {notEmpty: {msg: '-> Missing studentId'}}
        },
        firstname: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing firstname'}},
			allowNull: false
		},
		lastname: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing lastname'}},
			allowNull: false
		},
		dateOfBirth: {
			type: DataTypes.DATEONLY,
			validate: {notEmpty: {msg: '-> Missing date of birth'}},
			allowNull: false
		}
	});
};

module.exports = Students;